import '../../all_file.dart';

part 'team_model.g.dart';

@JsonSerializable()
class TeamModel {
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'teamName')
  String teamName;
  @JsonKey(name: 'teamCode')
  String teamCode;
  @JsonKey(name: 'email')
  String email;
  @JsonKey(name: 'phone')
  String phone;
  @JsonKey(name: 'address')
  String address;
  @JsonKey(name: 'timeLimit')
  String timeLimit;
  @JsonKey(name: 'ipWifi')
  String ipWifi;
//  @JsonKey(name: 'isWorkOnSat')
//  String isWorkOnSat;

  TeamModel(
      this.id,
      this.teamName,
      this.teamCode,
      this.email,
      this.phone,
      this.address,
      this.timeLimit,
      this.ipWifi,
//      this.isWorkOnSat,
      );

  factory TeamModel.fromJson(Map<String, dynamic> json) =>
      _$TeamModelFromJson(json);
}
