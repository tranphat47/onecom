import '../../all_file.dart';

part 'config_model.g.dart';
@JsonSerializable()

class ConfigModel{
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'shiftworkName')
  String shiftworkName;
  @JsonKey(name: 'begin')
  String begin;
  @JsonKey(name: 'end')
  String end;
  @JsonKey(name: 'team_id')
  String team_id;

  ConfigModel(
      this.id,
      this.shiftworkName,
      this.begin,
      this.end,
      this.team_id,
      );
  factory ConfigModel.fromJson(Map<String, dynamic> json) =>
      _$ConfigModelFromJson(json);
}