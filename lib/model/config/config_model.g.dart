part of 'config_model.dart';

ConfigModel _$ConfigModelFromJson(Map<String, dynamic> json) {
  return ConfigModel(
    json['id'] as String,
    json['shiftworkName'] as String,
    json['begin'] as String,
    json['end'] as String,
    json['team_id'] as String,
  );
}

Map<String, dynamic> _$ConfigModelToJson(ConfigModel instance) => <String, dynamic>{
  'id': instance.id,
  'shiftworkName': instance.shiftworkName,
  'begin': instance.begin,
  'end': instance.end,
  'team_id': instance.team_id,
};