import '../../all_file.dart';

part 'regisShift_model.g.dart';

@JsonSerializable()
class RegisShiftModel {
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'user_id')
  String user_id;
  @JsonKey(name: 'shiftwork_id')
  String shiftwork_id;
  @JsonKey(name: 'shiftworkName')
  String shiftworkName;
  @JsonKey(name: 'date')
  String date;
  @JsonKey(name: 'isCheckin')
  int isCheckin;
  @JsonKey(name: 'isCheckout')
  int isCheckout;

  RegisShiftModel(
      this.id,
      this.user_id,
      this.shiftwork_id,
      this.shiftworkName,
      this.date,
      this.isCheckin,
      this.isCheckout,
      );

  factory RegisShiftModel.fromJson(Map<String, dynamic> json) =>
      _$RegisShiftModelFromJson(json);
}
