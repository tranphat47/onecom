// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'regisShift_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisShiftModel _$RegisShiftModelFromJson(Map<String, dynamic> json) {
  return RegisShiftModel(
    json['id'] as String,
    json['user_id'] as String,
    json['shiftwork_id'] as String,
    json['shiftworkName'] as String,
    json['date'] as String,
    int.parse(json['isCheckin']),
    int.parse(json['isCheckout']),
  );
}

Map<String, dynamic> _$RegisShiftModelToJson(RegisShiftModel instance) => <String, dynamic>{
  'id': instance.id,
  'user_id': instance.user_id,
  'shiftwork_id': instance.shiftwork_id,
  'shiftworkName': instance.shiftworkName,
  'date': instance.date,
  'isCheckin': instance.isCheckin,
  'isCheckout': instance.isCheckout,
};
