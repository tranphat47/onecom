import '../../all_file.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'firstName')
  String firstName;
  @JsonKey(name: 'lastName')
  String lastName;
  @JsonKey(name: 'dateOfBirth')
  String dateOfBirth;
  @JsonKey(name: 'phone')
  String phone;
  @JsonKey(name: 'avataUrl')
  String avataUrl;
  @JsonKey(name: 'jobTitle')
  String jobTitle;
  @JsonKey(name: 'email')
  String email;
  @JsonKey(name: 'username')
  String username;
  @JsonKey(name: 'password')
  String password;
  @JsonKey(name: 'role_id')
  String role_id;
  @JsonKey(name: 'team_id')
  String team_id;

  UserModel(
    this.id,
    this.firstName,
    this.lastName,
    this.dateOfBirth,
    this.phone,
    this.avataUrl,
    this.jobTitle,
    this.email,
    this.username,
    this.password,
    this.role_id,
    this.team_id,
  );

  Widget getAvatar(double size) {
    return ClipOval(
      child: Container(
        width: size,
        height: size,
        color: myTheme.greenPrimary,
        padding: EdgeInsets.all(2),
        child: ClipOval(
          child: (avataUrl != null)
              ? CachedNetworkImage(
                  imageUrl: avataUrl,
                  width: size - 2,
                  height: size - 2,
                  placeholder: (context, url) => SvgPicture.asset(
                    'assets/svg/ic_user.svg',
                  ),
                )
              : SvgPicture.asset(
                  'assets/svg/ic_user.svg',
                  width: size - 2,
                  height: size - 2,
                ),
        ),
      ),
    );
  }

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}
