// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel(
    json['id'] as String,
    json['firstName'] as String,
    json['lastName'] as String,
    json['dateOfBirth'] as String,
    json['phone'] as String,
    json['avataUrl'] as String,
    json['jobTitle'] as String,
    json['email'] as String,
    json['username'] as String,
    json['password'] as String,
    json['role_id'] as String,
    json['team_id'] as String,
  );
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'dateOfBirth': instance.dateOfBirth,
      'phone': instance.phone,
      'avataUrl': instance.avataUrl,
      'jobTitle': instance.jobTitle,
      'email': instance.email,
      'username': instance.username,
      'password': instance.password,
      'role_id': instance.role_id,
      'team_id': instance.team_id,
    };
