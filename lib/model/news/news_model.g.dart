// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsModel _$NewsModelFromJson(Map<String, dynamic> json) {
  return NewsModel(
    json['id'] as String,
    json['title'] as String,
    json['description'] as String,
    json['url'] as String,
    json['imgUrl'] as String,
    json['createBy'] as String,
    json['team_id'] as String,
  );
}

Map<String, dynamic> _$NewsModelToJson(NewsModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'url': instance.url,
      'imgUrl': instance.imgUrl,
      'createBy': instance.createBy,
      'team_id': instance.team_id,
    };
