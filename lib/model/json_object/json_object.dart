import 'package:json_annotation/json_annotation.dart';

part 'json_object.g.dart';

@JsonSerializable()
class JsonObject extends Object with _$JsonObjectSerializerMixin{
  @JsonKey(name: 'key')
  String key;

  @JsonKey(name: 'value')
  String value;

  JsonObject(
    this.key,
    this.value,
  );

  factory JsonObject.fromJson(Map<String, dynamic> json) => _$JsonObjectFromJson(json);
}
