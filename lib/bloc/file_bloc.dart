import '../all_file.dart';

class FileBloc {
  File image;

  StreamController _changeController = new StreamController.broadcast();

  Stream get changeStream => _changeController.stream;

  void dispose() {
    _changeController.close();
  }

  Future getImage() async {
    var _image;
    _image = await ImagePicker.pickImage(source: ImageSource.gallery);
    image = _image;
    _changeController.sink.add('Change');
  }
}

final fileBloc = FileBloc();
