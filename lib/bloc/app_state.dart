import 'package:one_com/bloc/panel_bloc.dart';
import 'package:one_com/bloc/validate_bloc.dart';
import 'package:one_com/model/config/config_model.dart';
import 'package:one_com/model/team/team_model.dart';

import '../all_file.dart';

class AppState {
  SharedPreferences prefs;
  String token = '';
  String _language = 'vn';

  UserModel currentUser;
  UserModel get getCurrentUser => currentUser;
  TeamModel teamInfo;
  RegisShiftModel regisToday;

  List<NewsModel> listNews;
  List<UserModel> listUser;
  List<ConfigModel> listShiftwork;
  List<RegisShiftModel> listShiftRegis;

  final validateBloc = ValidateBloc();
  final panelBloc = PanelBloc();


  String log = '';
  AppState() {
    SharedPreferences.getInstance().then((value) {
      prefs = value;
    });
  }

  void setCurrentUser(UserModel value) {
    currentUser = value;
  }

  String get getLanguage => _language;

  void setLanguage(lang) {
    print(lang);
    _language = lang;
  }
}

final appState = AppState();
