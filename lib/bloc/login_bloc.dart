import '../all_file.dart';

class LoginBloc {
  StreamController _usernameController = new StreamController.broadcast();
  StreamController _passController = new StreamController.broadcast();

  Stream get nameStream => _usernameController.stream;

  Stream get passStream => _passController.stream;

  TextEditingController usernameController = TextEditingController();
  TextEditingController passController = TextEditingController();

  Future<bool> login() async {
    var result = await api.login(
      name: usernameController.text,
      pass: passController.text,
    );
    if (result) {
      appState.prefs.setString('username', usernameController.text);
      appState.prefs.setString('password', passController.text);
    }
    return result;
  }

  void dispose() {
    _usernameController.close();
    _passController.close();
  }

  bool isValidUser(String username) {
    return true;
    if (username.lastIndexOf('@') == -1) return false;
    return true;
  }
}

final loginBloc = LoginBloc();
