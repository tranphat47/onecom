import '../all_file.dart';

class NewsBloc {
  StreamController _urlController = new StreamController.broadcast();

  Stream get urlStream => _urlController.stream;

  StreamController _titleController = new StreamController.broadcast();

  Stream get titleStream => _titleController.stream;

  StreamController _descriptionController = new StreamController.broadcast();

  Stream get descriptionStream => _descriptionController.stream;

  TextEditingController urlController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  bool validate() {
    bool result = true;
    String empty = Languages.get('LBL_NOT_EMPTY');
    String url = urlController.text;
    // Validate url
    if (url == '') {
      _urlController.sink.addError(empty);
      result = false;
    } else if (!(url.contains('.com') ||
        url.contains('.vn') ||
        url.contains('http'))) {
      _urlController.sink.addError(Languages.get('LBL_MALFORMED'));
      result = false;
    } else {
      _urlController.sink.add('');
    }
    // Validate title
    if (titleController.text == '') {
      _titleController.sink.addError(empty);
      result = false;
    } else {
      _titleController.sink.add('');
    }
    // Validate description
    if (descriptionController.text == '') {
      _descriptionController.sink.addError(empty);
      result = false;
    } else {
      _descriptionController.sink.add('');
    }
    return result;
  }

  Future<bool> create() async {
    var result = await api.createNews(
      url: urlController.text,
      title: titleController.text,
      description: descriptionController.text,
      imgUrl: fileBloc.image,
      createBy: '',
      team_id: appState.currentUser.team_id,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> update(String id) async {
    var result = await api.updateNews(
      id: id,
      url: urlController.text,
      title: titleController.text,
      description: descriptionController.text,
      imgUrl: fileBloc.image,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> delete(String id) async {
    var result = await api.deleteNews(id);
    if (result) api.reload();
    return result;
  }

  void dispose() {
    _urlController.close();
    _titleController.close();
    _descriptionController.close();
  }
}

final newsBloc = NewsBloc();
