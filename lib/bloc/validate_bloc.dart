import 'dart:async';

import '../all_file.dart';

class ValidateBloc {
  bool _validate = false;
  dynamic _model;

  dynamic get getModel => _model;

  bool get canSave => _validate;

  set canSave(bool value) {
    _validate = value;
  }

  StreamController _validateController = new StreamController.broadcast();

  Stream get validateStream => _validateController.stream;

  void dispose() {
    _validateController.close();
  }

  void updateModel(dynamic model) {
    _model = model;
    checkValidate();
  }

  void checkValidate() {
    bool canSave = _model.validate;
    _validateController.sink.add(canSave);
    _validate = canSave;
  }
}
