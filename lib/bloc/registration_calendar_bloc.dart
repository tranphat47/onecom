import 'package:one_com/model/config/config_model.dart';

import '../all_file.dart';

class RegisCalendarBloc {
  DateTime dateRegis;

  ConfigModel selectedShift;

  StreamController _changeController = new StreamController.broadcast();

  Stream get changeStream => _changeController.stream;

  void dispose() {
    _changeController.close();
  }

  void setTempShift(ConfigModel model) {
    if (selectedShift == model)
      clearShift();
    else
      selectedShift = model;
    _changeController.sink.add('Change');
  }

  void setShift(ConfigModel model) {
    selectedShift = model;
    _changeController.sink.add('Change');
  }

  void clearShift() {
    selectedShift = null;
    _changeController.sink.add('Clear');
  }

  void setDateRegis(DateTime date) {
    dateRegis = date;
    _changeController.sink.add('Change');
  }

  Color colorButton() {
    Color colorButtonCheck;
    if (getModel().isCheckin == 0) {
      colorButtonCheck = myTheme.greenSecondary;
    } else if (getModel().isCheckout == 0) {
      colorButtonCheck = Colors.blueAccent;
    } else {
      colorButtonCheck = myTheme.gray;
    }
    return colorButtonCheck;
  }

  String contentButton() {
    String contentButtonCheck;
    if (getModel().isCheckin == 0) {
      contentButtonCheck = Languages.get('LBL_CHECKIN');
    } else if (getModel().isCheckout == 0) {
      contentButtonCheck = Languages.get('LBL_CHECKOUT');
    } else {
      contentButtonCheck = Languages.get('LBL_DONE');
    }
    return contentButtonCheck;
  }

  RegisShiftModel getModel() {
    RegisShiftModel model;
    appState.listShiftRegis.forEach((element) {
      DateTime date = DateTime.parse(element.date);
      if (SimpleUtil.dayIsEqual(
          date.millisecondsSinceEpoch, DateTime.now().millisecondsSinceEpoch)) {
        model = element;
      }
    });
    return model;
  }

  Future<bool> create() async {
    var result = await api.createRegisShift(
      user_id: appState.currentUser.id,
      shiftwork_id: selectedShift.id,
      shiftworkName: selectedShift.shiftworkName,
      date: dateRegis,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> delete() async {
    var result = await api.deleteRegisShift(
      id: getModelSelected(),
    );
    return result;
  }

  Future<bool> cancelShift() async {
    var result = await api.deleteRegisShift(
      id: getModelSelected(),
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> checkin() async {
    RegisShiftModel model;
    var result;
    appState.listShiftRegis.forEach((element) {
      DateTime date = DateTime.parse(element.date);
      if (SimpleUtil.dayIsEqual(
          date.millisecondsSinceEpoch, DateTime.now().millisecondsSinceEpoch)) {
        model = element;
      }
    });
    if (model != null) {
      if (model.isCheckin == 0) {
        result = await api.updateRegisShift(
          id: model.id,
          isCheckin: int.parse('1'),
        );
        if (result) api.reload();
      } else if (model.isCheckin == 1 && model.isCheckout == 0) {
        result = await api.updateRegisShift(
          id: model.id,
          isCheckout: int.parse('1'),
        );
        if (result) api.reload();
      }
    }
    return result;
  }

  String getModelSelected() {
    RegisShiftModel model;
    appState.listShiftRegis.forEach((element) {
      DateTime date = DateTime.parse(element.date);
      if (SimpleUtil.dayIsEqual(
          date.millisecondsSinceEpoch, dateRegis.millisecondsSinceEpoch)) {
        model = element;
      }
    });
    if (model != null)
      return model.id;
    else
      return null;
  }

  RegisShiftModel getModelToday() {
    RegisShiftModel model;
    appState.listShiftRegis.forEach((element) {
      DateTime date = DateTime.parse(element.date);
      if (SimpleUtil.dayIsEqual(
          date.millisecondsSinceEpoch, DateTime.now().millisecondsSinceEpoch)) {
        model = element;
      }
    });
    if (model != null)
      return model;
    else
      return null;
  }
}

final RegisCalendarBloc checkinBloc = RegisCalendarBloc();
