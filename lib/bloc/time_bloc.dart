import '../all_file.dart';

class TimeBloc {
  DateTime _selectedTimeBegin;

  DateTime _selectedTimeEnd;

  StreamController _changeTimeBeginController =
      new StreamController.broadcast();

  Stream get changeTimeBeginStream => _changeTimeBeginController.stream;

  StreamController _changeTimeEndController = new StreamController.broadcast();

  Stream get changeTimeEndStream => _changeTimeEndController.stream;

  DateTime get selectedTimeBegin =>
      (_selectedTimeBegin != null) ? _selectedTimeBegin : null;

  DateTime get selectedTimeEnd =>
      (_selectedTimeEnd != null) ? _selectedTimeEnd : null;

  String timeBegin;
  String timeEnd;

  void dispose() {
    _changeTimeBeginController.close();
    _changeTimeEndController.close();
  }

  void setSelectedTimeBegin(DateTime timeBegin) {
    _selectedTimeBegin = timeBegin;
    _changeTimeBeginController.sink.add('Change');
  }

  void setSelectedTimeEnd(DateTime timeEnd) {
    _selectedTimeEnd = timeEnd;
    _changeTimeEndController.sink.add('Change');
  }

  void clear(){
    _selectedTimeBegin = null;
    _selectedTimeEnd = null;
  }

  String getSelectedTimeBeginString() {
    var hours = '';
    var minutes = '';
    var time = '';
    hours = (selectedTimeBegin.hour < 10)
        ? '0' + selectedTimeBegin.hour.toString()
        : selectedTimeBegin.hour.toString();
    minutes = (selectedTimeBegin.minute < 10)
        ? '0' + selectedTimeBegin.minute.toString()
        : selectedTimeBegin.minute.toString();
    time = hours + ':' + minutes;
    return time;
  }

  String getSelectedTimeEndString() {
    var hours = '';
    var minutes = '';
    var time = '';
    hours = (selectedTimeEnd.hour < 10)
        ? '0' + selectedTimeEnd.hour.toString()
        : selectedTimeEnd.hour.toString();
    minutes = (selectedTimeEnd.minute < 10)
        ? '0' + selectedTimeEnd.minute.toString()
        : selectedTimeEnd.minute.toString();
    time = hours + ':' + minutes;
    return time;
  }

  String setTimeBegin() {
    TimeOfDay _timeBegin = TimeOfDay(
        hour: int.parse(timeBegin.split(":")[0]),
        minute: int.parse(timeBegin.split(":")[1]));
    return getStringTimeOfDay(_timeBegin);
  }

  String setTimeEnd() {
    TimeOfDay _timeEnd = TimeOfDay(
        hour: int.parse(timeEnd.split(":")[0]),
        minute: int.parse(timeEnd.split(":")[1]));
    return getStringTimeOfDay(_timeEnd);
  }

}

final timeBloc = TimeBloc();
