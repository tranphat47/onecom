import '../all_file.dart';

class CreateShiftwork {
  TextEditingController popupValue = TextEditingController();

  Widget _buildPopupCreate(
      ConfigModel model, BuildContext context, scaffoldKey) {
    shiftworkBloc.shiftworkController.text = model?.shiftworkName ?? '';
    timeBloc.timeBegin = model?.begin ?? '00:00:00';
    timeBloc.timeEnd = model?.end ?? '00:00:00';
//    shiftworkBloc.beginTimeController =
//        (model?.begin != '' && model?.begin != null)
//            ? DateTime.parse(model?.begin)
//            : null;
//    timeBloc.setSelectedTimeBegin(shiftworkBloc?.beginTimeController ?? null);
//
//    shiftworkBloc.endTimeController = (model?.end != '' && model?.end != null)
//        ? DateTime.parse(model?.end)
//        : null;
//    timeBloc.setSelectedTimeEnd(shiftworkBloc?.endTimeController ?? null);

    String label =
        (model == null) ? 'LBL_CREATE_SHIFTWORK' : 'LBL_EDIT_SHIFTWORK';
    String getLabel = Languages.get(label);
    String delete = Languages.get('LBL_DELETE');

    return StreamBuilder(
        stream: null,
        builder: (context, snapshot) {
          return Container(
            child: Column(
              children: <Widget>[
                (model == null)
                    ? CustomTextField(
                        label: 'LBL_SHIFTWORK_NAME',
                        required: true,
                        hint: 'LBL_SHIFTWORK_NAME_HINT',
                        controller: shiftworkBloc.shiftworkController,
                        shortField: false,
                      )
                    : Container(),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CustomTextField(
                      label: 'LBL_BEGIN_TIME',
                      required: true,
                      customTextFieldType: CustomTextFieldType.TIME_PICKER,
                    ),
                    CustomTextField(
                      label: 'LBL_END_TIME',
                      required: true,
                      customTextFieldType: CustomTextFieldType.TIME_PICKER_END,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CustomOverlayWidget(
                      onTap: () async {
                        if (shiftworkBloc.validate()) {
                          bool result = (model == null)
                              ? await shiftworkBloc.create()
                              : await shiftworkBloc.update(model.id);
                          if (result) {
                            myTheme.showSuccessNoti(scaffoldKey, '$getLabel');
                            Navigator.of(context).pop();
                            reloadBloc.reload();
                          } else {
                            myTheme.showErrorNoti(null, '$getLabel');
                          }
                        }
                      },
                      borderRadius: 20,
                      margin: EdgeInsets.only(top: 30),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      backgroundColor: myTheme.greenPrimary,
                      child: Center(
                          child: Text(
                        getLabel,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                    ),
//                    (model == null)
//                        ? Container()
//                        : CustomOverlayWidget(
//                            onTap: () async {
//                              bool result =
//                                  await shiftworkBloc.delete(model.id);
//                              if (result) {
//                                myTheme.showSuccessNoti(
//                                    scaffoldKey, '$delete ');
//                                Navigator.of(context).pop();
//                                reloadBloc.reload();
//                              } else {
//                                myTheme.showErrorNoti(null, '$delete ');
//                              }
//                            },
//                            isDelete: true,
//                            borderRadius: 20,
//                            margin: EdgeInsets.only(top: 30, left: 10),
//                            padding: EdgeInsets.symmetric(
//                                horizontal: 20, vertical: 10),
//                            backgroundColor: Colors.red,
//                            child: Center(
//                                child: Text(
//                              delete,
//                              style:
//                                  TextStyle(color: Colors.white, fontSize: 20),
//                            )),
//                          ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  showEditConfigPopup(BuildContext context, {ConfigModel model, scaffoldKey}) {
    myTheme.openAddEntryDialog(context,
        label: Languages.get('LBL_SHIFTWORK'),
        title: Container(),
        body: _buildPopupCreate(model, context, scaffoldKey));
  }
}
