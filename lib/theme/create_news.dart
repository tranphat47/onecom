import '../all_file.dart';

class CreateNews {
  TextEditingController popupValue = TextEditingController();

  Widget _buildPopupCreate(NewsModel model, BuildContext context, scaffoldKey) {
    newsBloc.urlController.text = model?.url ?? '';
    newsBloc.titleController.text = model?.title ?? '';
    newsBloc.descriptionController.text = model?.description ?? '';

    String label = (model == null) ? 'LBL_CREATE_NEWS' : 'LBL_EDIT_NEWS';
    String getLabel = Languages.get(label);
    String delete = Languages.get('LBL_DELETE');

    ProgressDialog pr;
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: Languages.get('LBL_PLEASE_WAIT'));

    return StreamBuilder(
      stream: reloadBloc.reloadStream,
      builder: (context, snapshot) {
        return Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              CustomTextField(
                required: true,
                label: 'LBL_NEWS_TITLE',
                hint: 'LBL_NEWS_TITLE_HINT',
                controller: newsBloc.titleController,
                stream: newsBloc.titleStream,
                shortField: false,
              ),
              SizedBox(
                height: 30,
              ),
              CustomTextField(
                required: true,
                label: 'LBL_NEWS_DES',
                hint: 'LBL_NEWS_DES_HINT',
                controller: newsBloc.descriptionController,
                stream: newsBloc.descriptionStream,
                shortField: false,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CustomOverlayWidget(
                    onTap: () async {
                      if (newsBloc.validate()) {
                        pr.show();
                        bool result = (model == null)
                            ? await newsBloc.create()
                            : await newsBloc.update(model.id);
                        if (result) {
                          Future.delayed(Duration(seconds: 0))
                              .then((value) {
                            pr.hide().whenComplete(() {
                              api.reload().then((value) {
                                Navigator.of(context).pop();
                                fileBloc.image = null;
                              });
                            });
                          });
                          myTheme.showSuccessNoti(scaffoldKey, '$getLabel');
                        } else {
                          Future.delayed(Duration(seconds: 0))
                              .then((value) {
                            pr.hide().whenComplete(() {
                              myTheme.showErrorNoti(null, '$getLabel');
                            });
                          });
                        }
                      }
                    },
                    borderRadius: 20,
                    margin: EdgeInsets.only(top: 30),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    backgroundColor: myTheme.greenPrimary,
                    child: Center(
                      child: Text(
                        getLabel,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                  (model == null)
                      ? Container()
                      : CustomOverlayWidget(
                          onTap: () async {
                            bool result = await newsBloc.delete(model.id);
                            if (result) {
                              myTheme.showSuccessNoti(scaffoldKey, '$delete ');
                              Navigator.of(context).pop();
                              reloadBloc.reload();
                            } else {
                              myTheme.showErrorNoti(null, '$delete ');
                            }
                          },
                          isDelete: true,
                          borderRadius: 20,
                          margin: EdgeInsets.only(top: 30, left: 10),
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          backgroundColor: Colors.red,
                          child: Center(
                              child: Text(
                            delete,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          )),
                        ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildTitlePopup(NewsModel model, BuildContext context, scaffoldKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        (model == null)
            ? myTheme.imagePicker(myTheme.greenPrimary, 'ic_news', 80)
            : CustomAvatar(
                size: 80,
                defaultSvg: 'ic_news',
                avatarUrl: model.imgUrl,
                picker: true,
              ),
        Container(),
        CustomTextField(
          required: true,
          label: 'LBL_NEWS_URL',
          hint: 'LBL_NEWS_URL_HINT',
          controller: newsBloc.urlController,
          stream: newsBloc.urlStream,
        ),
      ],
    );
  }

  showEditNewsPopup(BuildContext context, {NewsModel model, scaffoldKey}) {
    myTheme.openAddEntryDialog(context,
        label: Languages.get('LBL_NEWS'),
        title: _buildTitlePopup(model, context, scaffoldKey),
        body: _buildPopupCreate(model, context, scaffoldKey));
  }
}
