import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:one_com/theme/create_team.dart';
import 'change_password.dart';
import 'create_shiftwork.dart';
import 'create_user.dart';
import 'create_news.dart';

import '../all_file.dart';

class MyTheme with CreateUser, CreateTeam, CreateNews, CreateShiftwork, ChangePassword {
  Color lightGrey = Color(0xfff3f3f4);
  Color purple = Color(0xff6764f1);
  Color gray = Colors.grey;
  Color greenPrimary = Color(0xff27947f);
  Color greenSecondary = Color(0xff00bab9);
  Color orange = Color(0xfffeb01c);
  Color backgroundColor = Color(0xFFf5f5f5);
  Color shadowColor = Color(0xffc7c5c4);
  Color white = Colors.white;
  Color black = Colors.black54;
  double borderRadius = 30;

  List<String> symbol = [
    '~',
    '`',
    '!',
    '@',
    '#',
    '%',
    '^',
    '&',
    '*',
    '(',
    ')',
    '-',
    '_',
    '+',
    '='
  ];

  ProgressDialog pr;

  BoxDecoration get defaultDecoration => new BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: myTheme.shadowColor,
            blurRadius: 2.0,
            spreadRadius: 0.5,
            offset: Offset(
              0,
              2.0,
            ),
          ),
        ],
        color: Colors.white,
      );

  final GlobalKey<ScaffoldState> dialogScaffoldKey =
      new GlobalKey<ScaffoldState>();

  Widget title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'O',
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: greenPrimary,
          ),
          children: [
            TextSpan(
              text: 'ne',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: 'C',
              style: TextStyle(color: greenPrimary, fontSize: 30),
            ),
            TextSpan(
              text: 'om',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
          ]),
    );
  }

  void showBottomSheet({BuildContext context, Widget child}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: child,
          );
        });
  }

  Widget buildInputField(
    double width,
    TextEditingController controller,
    String label,
    bool obscureText,
    Stream stream,
    String hintText,
    TextInputType keyboardType,
  ) {
    return Container(
      width: width,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: greenSecondary),
          ),
          SizedBox(
            height: 10,
          ),
          StreamBuilder(
              stream: stream,
              builder: (context, snapshot) {
                return TextFormField(
                  controller: controller,
                  obscureText: obscureText,
                  style: TextStyle(fontSize: 15),
                  keyboardType: keyboardType,
                  decoration: new InputDecoration(
                    hintText: hintText,
                    errorText: (snapshot.hasError) ? snapshot.error : null,
                    filled: true,
                    fillColor: lightGrey,
                    contentPadding: const EdgeInsets.only(
                        left: 15.0, bottom: 15.0, top: 15.0),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      borderSide: BorderSide(color: greenSecondary),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: lightGrey),
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }

  Widget noDataAvailable() {
    return Center(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 40,
          ),
          Container(
            width: 200,
            child: Image.asset('assets/img/no_data.png'),
          ),
          Text(
            Languages.get('LBL_NO_DATA_AVAILABLE'),
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildLoadingWidget({bool withBackground = false}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SpinKitThreeBounce(
          color: withBackground ? Colors.white : greenPrimary,
          size: 30,
        ),
        SizedBox(
          height: 20,
        ),
        FadeAnimatedTextKit(
          text: [
            Languages.get('LBL_PLEASE_WAIT'),
          ],
          textStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w300,
            decoration: TextDecoration.none,
            color: withBackground ? Colors.white : greenPrimary,
          ),
        ),
      ],
    );
  }

  Widget imagePicker(
    Color color,
    String defaultImage,
    double size,
  ) {
    return StreamBuilder(
        stream: fileBloc.changeStream,
        builder: (context, snapshot) {
          return GestureDetector(
            onTap: () {
              fileBloc.getImage();
            },
            child: ClipOval(
              child: Container(
                padding: const EdgeInsets.all(1),
                color: color,
                child: ClipOval(
                  child: Container(
                    child: (fileBloc.image == null)
                        ? SvgPicture.asset(
                            'assets/svg/$defaultImage.svg',
                          )
                        : Image.file(
                            fileBloc.image,
                            fit: BoxFit.cover,
                            width: size - 1,
                            height: size - 1,
                          ),
                    width: size - 1,
                    height: size - 1,
                  ),
                ),
              ),
            ),
          );
        });
  }

  void openAddEntryDialog(context,
      {Widget title, Widget body, String label = ''}) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return Scaffold(
            key: dialogScaffoldKey,
            backgroundColor: backgroundColor,
            appBar: new AppBar(
              title: Text(
                label,
                style: TextStyle(color: white),
              ),
              backgroundColor: greenPrimary,
            ),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  title,
                  Expanded(
                    child: body,
                  )
                ],
              ),
            ),
          );
        },
        fullscreenDialog: true));
  }

  void showSuccessNoti(scaffoldKey, String label) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.green,
      content: Row(
        children: <Widget>[
          Icon(
            Icons.check,
            color: Colors.white,
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            '$label ' + Languages.get('LBL_SUCCESSFULLY'),
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
          ),
        ],
      ),
      duration: Duration(seconds: 3),
    ));
  }

  void showErrorNoti(scaffoldKey, String label) {
    var key = (scaffoldKey == null) ? dialogScaffoldKey : scaffoldKey;
    key.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red,
      content: Row(
        children: <Widget>[
          Icon(
            Icons.error_outline,
            color: Colors.white,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              (appState.log == '')
                  ? '$label ' + Languages.get('LBL_FAILURE')
                  : appState.log,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
          ),
        ],
      ),
      duration: Duration(seconds: 3),
    ));
  }

  void showMadeNoti(scaffoldKey, String label) {
    var key = (scaffoldKey == null) ? dialogScaffoldKey : scaffoldKey;
    key.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.blueAccent,
      content: Row(
        children: <Widget>[
          Icon(
            Icons.warning,
            color: Colors.yellow,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              '$label',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
          ),
        ],
      ),
      duration: Duration(seconds: 3),
    ));
  }

  void showErrorNotiWithoutSub(scaffoldKey, String label) {
    var key = (scaffoldKey == null) ? dialogScaffoldKey : scaffoldKey;
    key.currentState.showSnackBar(SnackBar(
      backgroundColor: Colors.red,
      content: Row(
        children: <Widget>[
          Icon(
            Icons.error_outline,
            color: Colors.white,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              (appState.log == '')
                  ? '$label'
                  : appState.log,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
          ),
        ],
      ),
      duration: Duration(seconds: 3),
    ));
  }


}

final MyTheme myTheme = MyTheme();
