import '../../../all_file.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => new _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  Widget build(BuildContext context) {
    String url = ModalRoute.of(context).settings.arguments;
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: CustomScaffold(
        label: 'LBL_NEWS',
        body: (url != null)
            ? Container(
                color: myTheme.backgroundColor,
                child: Column(
                  children: [
                    Expanded(
                        child: WebviewScaffold(
                            hidden: true,
                            initialChild: Container(
                              color: myTheme.backgroundColor,
                              constraints: BoxConstraints.expand(),
                              child: myTheme.buildLoadingWidget(),
                            ),
                            appCacheEnabled: true,
                            withJavascript: true,
                            url: url))
                  ],
                ),
              )
            : Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      width: 200,
                      child: Image.asset('assets/img/no_data.png'),
                    ),
                    Text(
                      Languages.get('LBL_NO_DATA'),
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
