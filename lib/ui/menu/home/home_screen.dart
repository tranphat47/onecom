import 'package:one_com/widget/awesome_list_item.dart';

import '../../../all_file.dart';
import 'dart:math';

var COLORS = [
  Color(0xFF9AE889),
  Color(0xFFFFF6A3),
  Color(0xFFFFC2E2),
  Color(0xFFE8BE89),
  Color(0xFFFF9D96)
];

var IMAGE = [
  "https://picsum.photos/100?random",
  "https://picsum.photos/125?random",
  "https://picsum.photos/150?random",
  "https://picsum.photos/175?random",
  "https://picsum.photos/200?random",
  "https://picsum.photos/225?random",
  "https://picsum.photos/250?random",
  "https://picsum.photos/275?random",
  "https://picsum.photos/300?random",
  "https://picsum.photos/325?random",
];

class MainFeed extends StatefulWidget {
  @override
  _MainFeedState createState() => _MainFeedState();
}

class _MainFeedState extends State<MainFeed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        color: myTheme.backgroundColor,
        child: Column(
          children: <Widget>[
            _buildAppBar(),
            _buildListNews(),
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar() {
    return Container(
      width: getWidth(context, 1, null),
      height: getHeight(context, 0.23, null),
      color: myTheme.greenPrimary,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//            Align(alignment: Alignment.centerLeft, child: _buildAppName()),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _buildAppName(),
                _buildSelectLanguage(context, appState.getLanguage),
              ],
            ),
            _buildTeamInfo(),
          ],
        ),
      ),
    );
  }

  Widget _buildAppName() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, '/teaminfo'),
      child: Container(
        margin: EdgeInsets.only(
          top: getHeight(context, 0.06, null),
        ),
        width: getWidth(context, 0.3, null),
        height: 30,
        decoration: BoxDecoration(
          color: myTheme.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(),
            Text(
              'OneCom',
              style: TextStyle(
                fontSize: 14,
                color: myTheme.greenPrimary,
                fontWeight: FontWeight.bold,
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: myTheme.greenPrimary,
              size: 14,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTeamInfo() {
    return Container(
      margin: EdgeInsets.only(top: getHeight(context, 0.03, null), left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            appState.teamInfo.teamName,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 22,
              color: myTheme.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListNews() {
    return Expanded(
      child: Container(
        child: ListView.builder(
          shrinkWrap: true,
          padding: const EdgeInsets.all(0.0),
          scrollDirection: Axis.vertical,
          primary: true,
          itemCount: appState.listNews.length,
          itemBuilder: (BuildContext content, int index) {
            return AwesomeListItem(
              title: appState.listNews[index].title,
              content: appState.listNews[index].description,
              color: COLORS[new Random().nextInt(5)],
//              image: (appState.listNews[index].imgUrl != null)
//                  ? appState.listNews[index].imgUrl
//                  : IMAGE[new Random().nextInt(10)],
              image: appState.listNews[index].imgUrl,
              url: appState.listNews[index].url,
            );
          },
        ),
      ),
    );
  }

  Widget _buildSelectLanguage(BuildContext context, String language) {
    return GestureDetector(
      onTap: () => _showModalSelectLanguage(context, language),
      child: Container(
        margin: EdgeInsets.only(
          top: getHeight(context, 0.06, null),
          right: 10,
        ),
        child: ClipOval(
          child: Container(
            color: myTheme.white,
            padding: EdgeInsets.all(1),
            child: ClipOval(
              child: Container(
                color: myTheme.white,
                padding: EdgeInsets.all(8),
                child: Text(
                  (language == 'en') ? "EN" : "VN",
                  style: TextStyle(
                    color: myTheme.greenPrimary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _showModalSelectLanguage(BuildContext context, String language) {
    bool _isEn = (language == 'en') ? true : false;
    bool _isVn = (language == 'vn') ? true : false;

    myTheme.showBottomSheet(
        context: context,
        child: Wrap(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20)),
                color: Colors.white,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0, 1.0),
                    blurRadius: 20.0,
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 15, 10, 20),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        Languages.get('LBL_CHANGE_LANGUAGE'),
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 8, 8, 8),
                      child: Text(
                        Languages.get('LBL_SUB_CHANGE'),
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    new ListTile(
                        leading: new Image.asset(
                          "assets/img/en_us.png",
                          width: 35,
                        ),
                        title: new Text(
                          'English',
                          style: TextStyle(
                            color:
                                (_isEn) ? myTheme.greenPrimary : Colors.black,
                            fontWeight:
                                (_isEn) ? FontWeight.bold : FontWeight.normal,
                          ),
                        ),
                        trailing: (_isEn)
                            ? Icon(
                                Icons.check_circle_outline,
                                color: myTheme.greenPrimary,
                              )
                            : null,
                        onTap: () {
                          setState(() {
                            appState.setLanguage('en');
                          });
                          Navigator.pop(context);
                        }),
                    new ListTile(
                        leading: new Image.asset(
                          "assets/img/vn_vn.png",
                          width: 35,
                        ),
                        title: new Text(
                          'Tiếng Việt',
                          style: TextStyle(
                            color:
                                (_isVn) ? myTheme.greenPrimary : Colors.black,
                            fontWeight:
                                (_isVn) ? FontWeight.bold : FontWeight.normal,
                          ),
                        ),
                        trailing: (_isVn)
                            ? Icon(
                                Icons.check_circle_outline,
                                color: myTheme.greenPrimary,
                              )
                            : null,
                        onTap: () {
                          setState(() {
                            appState.setLanguage('vn');
                            reloadBloc.reload();
                          });
                          Navigator.pop(context);
                        }),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
