import '../../../all_file.dart';

class ManageScreen extends StatefulWidget {
  @override
  _ManageScreenState createState() => _ManageScreenState();
}

class _ManageScreenState extends State<ManageScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: myTheme.backgroundColor,
      appBar: AppBar(
        backgroundColor: myTheme.greenPrimary,
        title: Text(
          Languages.get('LBL_MANAGE'),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.0,
            color: Colors.white,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(myTheme.borderRadius),
          ),
        ),
      ),
      floatingActionButton: CircularMenu(
        alignment: Alignment.bottomRight,
        radius: 100,
        curve: Curves.bounceOut,
        startingAngleInRadian: 3.5,
        endingAngleInRadian: 4.5,
        toggleButtonOnPressed: () {},
        toggleButtonColor: myTheme.greenSecondary,
        toggleButtonElevation: 4.0,
        toggleButtonIconColor: Colors.white,
        toggleButtonMargin: 10.0,
        toggleButtonPadding: 10.0,
        toggleButtonSize: 40.0,
        items: [
          CircularMenuItem(
            onTap: () =>
                myTheme.showEditUserPopup(context, scaffoldKey: _scaffoldKey),
            icon: Icons.person_add,
            color: Color(0xFFBD1795),
            elevation: 4.0,
            iconColor: Colors.white,
            iconSize: 30.0,
            margin: 10.0,
            padding: 10.0,
          ),
          CircularMenuItem(
            onTap: () =>
                myTheme.showEditNewsPopup(context, scaffoldKey: _scaffoldKey),
            icon: Icons.note_add,
            color: Color(0xFFBDA117),
            elevation: 4.0,
            iconColor: Colors.white,
            iconSize: 30.0,
            margin: 10.0,
            padding: 10.0,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _itemManagement('LBL_TEAM', Icons.group, 'team_management'),
            _itemManagement(
                'LBL_USER', Icons.account_circle, 'user_management'),
            _itemManagement('LBL_NEWS', Icons.description, 'news_management'),
            _itemManagement(
                'LBL_SHIFTWORK', Icons.settings, 'shiftwork_management'),
            _itemManagement('LBL_REPORT', Icons.insert_chart, 'reports'),
          ],
        ),
      ),
    );
  }

  Widget _itemManagement(String label, IconData icon, String link) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, '/$link'),
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        height: 60,
        width: getWidth(context, 1, null),
        decoration: BoxDecoration(
          color: myTheme.white,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: myTheme.shadowColor,
              blurRadius: 2.0,
              spreadRadius: 0.5,
              offset: Offset(
                0,
                2.0,
              ),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Icon(
                    icon,
                    size: 30,
                    color: myTheme.black,
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                Text(
                  Languages.get(label),
                  style: TextStyle(
                    color: myTheme.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Container(),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Icon(
                Icons.arrow_forward_ios,
                size: 20,
                color: myTheme.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
