import '../../../all_file.dart';

class User extends StatefulWidget {
  @override
  _UserState createState() => _UserState();
}

class _UserState extends State<User> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: StreamBuilder(
          stream: reloadBloc.reloadStream,
          builder: (context, snapshot) {
            return Stack(
              children: <Widget>[
                ClipPath(
                  child: Container(color: myTheme.greenPrimary),
                  clipper: getClipper(),
                ),
                (appState.currentUser.role_id == '1')
                    ? FractionallySizedBox(
                        alignment: Alignment.topCenter,
                        heightFactor: 0.17,
                        widthFactor: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () =>
                                  Navigator.of(context).pushNamed('/manage'),
                              child: IconButton(
                                icon: Icon(
                                  Icons.dashboard,
                                  color: Colors.white,
                                ),
                                iconSize: 30,
                              ),
                            )
                          ],
                        ),
                      )
                    : Container(),
                StreamBuilder(
                    stream: reloadBloc.reloadStream,
                    builder: (context, snapshot) {
                      return Positioned(
                        width: MediaQuery.of(context).size.width,
                        top: MediaQuery.of(context).size.height / 5,
                        child: Column(
                          children: <Widget>[
                            _buildAvatar(),
                            SizedBox(height: 50.0),
                            _buildEditInfo(),
                            SizedBox(height: 15.0),
                            Text(
                              appState.currentUser?.dateOfBirth ?? '',
                              style: TextStyle(
                                fontSize: 17.0,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              appState.currentUser?.jobTitle ?? '',
                              style: TextStyle(
                                fontSize: 17.0,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            SizedBox(height: 50.0),
                            _buildChangePassword(),
                            SizedBox(height: 15.0),
                            _buildLogout(),
                          ],
                        ),
                      );
                    })
              ],
            );
          }),
    );
  }

  Widget _buildEditInfo() {
    return GestureDetector(
      onTap: () => myTheme.showEditUserPopup(context,
          model: appState.currentUser, scaffoldKey: _scaffoldKey),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 25,
          ),
          Text(
            (appState.currentUser.firstName != null)
                ? appState.currentUser.firstName +
                    ' ' +
                    appState.currentUser.lastName
                : '',
            style: TextStyle(
              fontSize: 25.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.edit,
              color: myTheme.black,
              size: 25,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAvatar() {
    return Container(
        width: 150.0,
        height: 150.0,
        decoration: BoxDecoration(
            color: Colors.red,
            image: DecorationImage(
                image: (appState.currentUser.avataUrl != null)
                    ? NetworkImage(appState.currentUser.avataUrl)
                    : AssetImage('assets/img/user_default.jpeg'),
                fit: BoxFit.cover),
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
            boxShadow: [BoxShadow(blurRadius: 7.0, color: Colors.black)]));
  }

  Widget _buildEditProfile() {
    return Container(
      height: 30.0,
      width: 125.0,
      child: Material(
        borderRadius: BorderRadius.circular(20.0),
        shadowColor: myTheme.greenSecondary,
        color: myTheme.greenSecondary,
        elevation: 7.0,
        child: GestureDetector(
          onTap: () => myTheme.showEditUserPopup(context,
              model: appState.currentUser, scaffoldKey: _scaffoldKey),
          child: Center(
            child: Text(
              Languages.get('LBL_EDIT_PROFILE'),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildChangePassword() {
    return Container(
      height: 40.0,
      width: 130.0,
      child: Material(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.blueAccent,
        elevation: 7.0,
        child: GestureDetector(
          onTap: () => myTheme.showChangePasswordPopup(context),
          child: Center(
            child: Text(
              Languages.get('LBL_CHANGE_PASSWORD'),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLogout() {
    return Container(
      height: 40.0,
      width: 130.0,
      child: Material(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.red,
        elevation: 7.0,
        child: GestureDetector(
          onTap: () {
            api.logout();
            Navigator.pushNamedAndRemoveUntil(
                context, '/login', (Route<dynamic> route) => false);
          },
          child: Center(
            child: Text(
              Languages.get('LBL_LOGOUT'),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
