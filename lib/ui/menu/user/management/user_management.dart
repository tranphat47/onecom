import 'package:intl/intl.dart';

import '../../../../all_file.dart';

class UserManagement extends StatefulWidget {
  @override
  _UserManagementState createState() => _UserManagementState();
}

class _UserManagementState extends State<UserManagement> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: myTheme.backgroundColor,
      appBar: AppBar(
        backgroundColor: myTheme.greenPrimary,
        title: Text(
          Languages.get('LBL_USER_MANAGE'),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.0,
            color: Colors.white,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(myTheme.borderRadius),
          ),
        ),
      ),
      body: StreamBuilder(
          stream: reloadBloc.reloadStream,
          builder: (context, snapshot) {
            return Container(
              margin: EdgeInsets.only(top: 10),
              child: ListView.builder(
                itemCount: appState.listUser.length,
                itemBuilder: (context, index) => _buildUser(index),
              ),
            );
          }),
    );
  }

  Widget _buildUser(int index) {
    UserModel model = appState.listUser[index];
    final styleText = TextStyle(
      color: myTheme.black,
      fontSize: 13,
    );
    return GestureDetector(
      onTap: () {
        myTheme.showEditUserPopup(context,
            model: model, scaffoldKey: _scaffoldKey);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15),
        decoration: myTheme.defaultDecoration,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: ListTile(
            leading: model.getAvatar(50),
            title: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    model.firstName + ' ' + model.lastName,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: myTheme.greenSecondary,
                    ),
                  ),
                ),
//                Expanded(
//                  child: Text(
//                    (model.role_id == '1') ? ' - Admin' : ' - User',
//                    style: TextStyle(
//                      fontSize: 20,
//                      fontWeight: FontWeight.bold,
//                      color: myTheme.black,
//                    ),
//                  ),
//                ),
              ],
            ),
            subtitle: Column(
              children: <Widget>[
                SizedBox(height: 5,),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.cake,
                      color: myTheme.black,
                      size: 15,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      model?.dateOfBirth ?? Languages.get('LBL_NONE'),
                      style: styleText,
                    ),
                  ],
                ),
                SizedBox(height: 3,),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.phone,
                      color: myTheme.black,
                      size: 15,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      model?.phone ?? Languages.get('LBL_NONE'),
                      style: styleText,
                    ),
                  ],
                ),
                SizedBox(height: 3,),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.subtitles,
                      color: myTheme.black,
                      size: 15,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      model?.jobTitle ?? Languages.get('LBL_NONE'),
                      style: styleText,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
