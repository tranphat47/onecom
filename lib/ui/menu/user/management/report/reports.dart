import '../../../../../all_file.dart';

enum ReportType {
  SHIFT,
}

class Reports extends StatefulWidget {
  @override
  _ReportsState createState() => _ReportsState();
}

class _ReportsState extends State<Reports> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
        backgroundColor: myTheme.backgroundColor,
        label: 'LBL_REPORT',
        body: Container(
            margin: EdgeInsets.only(top: 10),
            child: Column(
              children: <Widget>[
                _buildReportItem('LBL_REPORT_SHIFT', 'LBL_REPORT_SHIFT_DES',
                    ReportType.SHIFT)
              ],
            ))
    );
  }

  Widget _buildReportItem(String title, String description, ReportType type) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pushNamed('/report', arguments: type),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15),
        decoration: myTheme.defaultDecoration,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: ListTile(
            leading: Container(
              width: 50,
              child: SvgPicture.asset('assets/svg/ic_report_shift.svg'),
            ),
            title: Text(
              Languages.get(title),
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: myTheme.greenSecondary,
              ),
            ),
            subtitle: Padding(
              padding: const EdgeInsets.only(top:3),
              child: Text(
                Languages.get(description),
                maxLines: 2,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
