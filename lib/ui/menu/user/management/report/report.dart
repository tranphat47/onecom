import '../../../../../all_file.dart';

class ReportDetail extends StatefulWidget {
  @override
  _ReportDetailState createState() => _ReportDetailState();
}

class _ReportDetailState extends State<ReportDetail> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ReportType typeReport;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    reportBloc.setDatetime(
        from: now.subtract(Duration(days: now.day - 1)), to: now);
  }

  @override
  Widget build(BuildContext context) {
    typeReport = ModalRoute.of(context).settings.arguments;
    return CustomScaffold(
      backgroundColor: myTheme.backgroundColor,
      label: 'LBL_REPORT_DETAIL',
      body: _buildReport(),
    );
  }

  Widget _buildReport() {
    switch (typeReport) {
      case ReportType.SHIFT:
        return _buildShiftReport();
        break;
    }
  }

  _buildDatePicker(BuildContext context, bool start) {
    return GestureDetector(
      onTap: () async {
        showRoundedDatePicker(
            context: context,
            locale: appState.getLanguage,
            initialDate: start ? reportBloc.dateStart : reportBloc.dateEnd,
            firstDate: DateTime(DateTime.now().year - 10),
            lastDate: DateTime(DateTime.now().year + 1),
            borderRadius: 16,
            textPositiveButton: Languages.get('LBL_CHOOSE'),
            textNegativeButton: Languages.get('LBL_CANCEL'),
            customWeekDays: (appState.getLanguage == 'vn')
                ? ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7',]
                : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            theme: ThemeData(
              primaryColor: myTheme.greenPrimary.withOpacity(0.8),
            ),
            styleDatePicker: MaterialRoundedDatePickerStyle(
              textStyleDayButton: TextStyle(fontSize: 20, color: Colors.orange),
              textStyleYearButton: TextStyle(
                fontSize: 35,
                color: Colors.white,
              ),
              textStyleDayHeader: TextStyle(
                  fontSize: 16,
                  color: myTheme.greenPrimary,
                  fontWeight: FontWeight.w600),
              textStyleCurrentDayOnCalendar: TextStyle(
                  fontSize: 20,
                  color: Colors.orange,
                  fontWeight: FontWeight.bold),
              textStyleDayOnCalendar:
                  TextStyle(fontSize: 16, color: myTheme.greenPrimary),
              textStyleDayOnCalendarSelected: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
              textStyleDayOnCalendarDisabled:
                  TextStyle(fontSize: 16, color: Colors.grey),
              textStyleMonthYearHeader: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
              paddingDatePicker: EdgeInsets.all(0),
              paddingMonthHeader: EdgeInsets.all(12),
              paddingActionBar: EdgeInsets.all(0),
              paddingDateYearHeader: EdgeInsets.only(left: 15, top: 10),
              sizeArrow: 30,
              colorArrowNext: Colors.white,
              colorArrowPrevious: Colors.white,
              textStyleButtonAction:
                  TextStyle(fontSize: 20, color: Colors.white),
              textStyleButtonPositive: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
              textStyleButtonNegative:
                  TextStyle(fontSize: 20, color: Colors.white.withOpacity(0.5)),
              decorationDateSelected: BoxDecoration(
                  color: Colors.orange[600], shape: BoxShape.circle),
              backgroundActionBar: myTheme.greenPrimary.withOpacity(0.8),
              backgroundHeaderMonth: myTheme.greenPrimary.withOpacity(0.8),
            ),
            styleYearPicker: MaterialRoundedYearPickerStyle(
              textStyleYear: TextStyle(
                fontSize: 20,
                color: myTheme.greenPrimary,
              ),
              textStyleYearSelected: TextStyle(
                  fontSize: 24,
                  color: myTheme.greenPrimary,
                  fontWeight: FontWeight.bold),
            )).then((dt) {
          if (start) {
            reportBloc.setDatetime(from: dt);
          } else {
            reportBloc.setDatetime(to: dt);
          }
          reloadBloc.reload();
        });
      },
      child: CustomOverlayWidget(
        borderRadius: 15,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            getDisplayDateTime(
              start ? reportBloc.dateStart : reportBloc.dateEnd,
            ),
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildShiftReport() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _buildTitleReport(),
          _buildReportTable(),
        ],
      ),
    );
  }

  _buildTitleReport() {
    return StreamBuilder(
        stream: reportBloc.dateStream,
        builder: (context, snapshot) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Text(
                    Languages.get('LBL_REPORT_SHIFT').toUpperCase(),
                    style: TextStyle(
                        color: myTheme.greenPrimary,
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Text(Languages.get('LBL_FROM') + ': ',
                        style: TextStyle(
                            color: myTheme.greenPrimary,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                    _buildDatePicker(context, true),
                    Spacer(),
                    Text(Languages.get('LBL_TO') + ': ',
                        style: TextStyle(
                            color: myTheme.greenPrimary,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                    _buildDatePicker(context, false)
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          );
        });
  }

  _buildReportTable() {
    return StreamBuilder(
        stream: reportBloc.dataStream,
        builder: (context, snapshot) {
          if (reportBloc.reportDetail == null) {
            return Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    width: 200,
                    child: Image.asset('assets/img/no_data.png'),
                  ),
                  Text(
                    Languages.get('LBL_NO_DATA'),
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            );
          } else {
            double height = 110.0 + (60.0 * reportBloc.reportDetail.length);
            return Container(
              height: height,
              margin: EdgeInsets.all(10),
              child: HorizontalDataTable(
                leftHandSideColumnWidth: 200,
                rightHandSideColumnWidth: 300,
                isFixedHeader: true,
                headerWidgets: _getTitleWidget(),
                leftSideItemBuilder: _generateFirstColumnRow,
                rightSideItemBuilder: _generateRightHandSideColumnRow,
                itemCount: reportBloc.reportDetail.length,
                leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
                rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
              ),
            );
          }
        });
  }

  List<Widget> _getTitleWidget() {
    return [
      Row(
        children: <Widget>[
          _getTitleItemWidget('LBL_NO', 50, index: 1),
          _getTitleItemWidget('LBL_FULL_NAME', 150),
        ],
      ),
      _getTitleItemWidget('LBL_JOB_TITLE', 100),
      _getTitleItemWidget('LBL_TOTAL_DAY', 100),
      _getTitleItemWidget('LBL_DOB', 100),
    ];
  }

  Widget _getTitleItemWidget(String label, double width, {int index}) {
    return Container(
      child: Text(Languages.get(label),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
      width: width,
      height: 60,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          border: Border(
              right: BorderSide(color: myTheme.greenPrimary),
              left: (index == 1)
                  ? BorderSide(color: myTheme.greenPrimary)
                  : BorderSide.none),
          color: myTheme.greenSecondary),
    );
  }

  Widget _getDataItemWidget(String value, double width, Alignment alignment,
      {bool borderLeft = true, borderRight = true}) {
    return Container(
      child: Text(
        value ?? '',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      width: width,
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 5),
      alignment: alignment,
      decoration: BoxDecoration(
          border: Border(
              right: (borderRight)
                  ? BorderSide(color: myTheme.greenPrimary)
                  : BorderSide.none,
              left: (borderLeft)
                  ? BorderSide(color: myTheme.greenPrimary)
                  : BorderSide.none)),
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    int no = index + 1;
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              child: Text(
                '$no',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              width: 50,
              height: 60,
              padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border(
                      right: BorderSide(color: myTheme.greenPrimary),
                      left: BorderSide(color: myTheme.greenPrimary))),
            ),
            Container(
              child: Text(
                reportBloc.reportDetail[index]?.fullName ?? '',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              width: 150,
              height: 60,
              padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                  border: Border(
                right: BorderSide(color: myTheme.greenPrimary),
              )),
            ),
          ],
        ),
        const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
      ],
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            _getDataItemWidget(
              reportBloc.reportDetail[index]?.jobTitle,
              100,
              Alignment.center,
              borderLeft: false,
            ),
            _getDataItemWidget(
              reportBloc.reportDetail[index]?.totalDay.toString(),
              100,
              Alignment.center,
              borderLeft: false,
            ),
            _getDataItemWidget(
              reportBloc.reportDetail[index]?.dateOfBirth,
              100,
              Alignment.centerLeft,
              borderLeft: false,
            ),
          ],
        ),
        const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),
      ],
    );
  }
}
