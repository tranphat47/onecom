import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_com/ui/menu/contacts/contacts_screen.dart';
import 'package:one_com/ui/menu/user/user_screen.dart';

import '../all_file.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Widget> _children = [
    MainFeed(),
    Contact(),
    Schedule(),
    User(),
  ];

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: reloadBloc.reloadStream,
      builder: (context, snapshot) {
        return Scaffold(
          key: _scaffoldKey,
          body: Container(
            child: _children[_selectedIndex],
          ),
          bottomNavigationBar: _bottomNavBar(),
        );
      }
    );
  }

  Widget _bottomNavBar() => StreamBuilder(
    stream: reloadBloc.reloadStream,
    builder: (context, snapshot) {
      return Container(
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
            ]),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
                child: GNav(
                    gap: 8,
                    activeColor: Colors.white,
                    iconSize: 24,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    duration: Duration(milliseconds: 500),
                    tabBackgroundColor: myTheme.greenPrimary,
                    tabs: [
                      GButton(
                        icon: LineIcons.home,
                        text: Languages.get('LBL_HOME'),
                      ),
                      GButton(
                        icon: LineIcons.group,
                        text: Languages.get('LBL_COMMUNITIES'),
                      ),
                      GButton(
                        icon: LineIcons.calendar,
                        text: Languages.get('LBL_CALENDAR'),
                      ),
                      GButton(
                        icon: LineIcons.user,
                        text: Languages.get('LBL_PROFILE'),
                      ),
                    ],
                    selectedIndex: _selectedIndex,
                    onTabChange: (index) {
                      setState(() {
                        _selectedIndex = index;
                      });
                    }),
              ),
            ),
          );
    }
  );
}
