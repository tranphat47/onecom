import 'all_file.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeAgo;
import 'package:http/http.dart' as http;

enum FormatType { DATE, TIME, DATE_TIME }

String getTimeAgo(String datetime) {
  final fifteenAgo = DateTime.parse(datetime);
  timeAgo.setLocaleMessages('vn', timeAgo.ViMessages());
  return timeAgo.format(fifteenAgo, locale: 'vn');
}

String getDisplayDateTime(dynamic dateTime,
    {FormatType type = FormatType.DATE}) {
  DateTime dt = (dateTime is DateTime) ? dateTime : DateTime.parse(dateTime);
  if (dateTime == '' || dateTime == null) return '';
  switch (type) {
    case FormatType.DATE:
      return DateFormat('dd-MM-yyyy').format(dt);
    case FormatType.TIME:
      return DateFormat.Hm().format(dt);
    case FormatType.DATE_TIME:
      return DateFormat('HH:mm dd-MM-yyyy').format(dt);
    default:
      return DateFormat('HH:mm dd-MM-yyyy').format(dt);
  }
}

void navigatorPushNamed(BuildContext context, String routeName, arguments) {
  Navigator.pushNamed(context, '/$routeName', arguments: arguments);
}

/// Show error notification
void showErrorNotification(context,
    {String title = 'Lỗi kết nối', String content}) {
  /* Flushbar(
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    backgroundColor: Color(0xFF303030),
    borderRadius: 8,
    margin: EdgeInsets.all(8),
    titleText: Text(
      title,
      style: TextStyle(
        color: Colors.white,
        fontSize: 18,
        fontWeight: FontWeight.w400,
      ),
    ),
    messageText: Text(
      content,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    icon: Icon(
      Icons.error_outline,
      size: 40.0,
      color: Colors.red[500],
    ),
    duration: Duration(seconds: 3),
    flushbarPosition: FlushbarPosition.TOP,
  )..show(context);*/
}

/// Show information notification
void showInformationNotification(context, title, content) {
  /* Flushbar(
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    backgroundColor: Color(0xFF303030),
    borderRadius: 8,
    margin: EdgeInsets.all(8),
    titleText: Text(
      title,
      style: TextStyle(
        color: Colors.white,
        fontSize: 18,
        fontWeight: FontWeight.w400,
      ),
    ),
    messageText: Text(
      content,
      style: TextStyle(
        color: Colors.white,
      ),
    ),
    icon: Icon(
      Icons.error_outline,
      size: 40.0,
      color: Colors.blue[500],
    ),
    duration: Duration(seconds: 3),
    flushbarPosition: FlushbarPosition.TOP,
  )..show(context);*/
}

double getWidth(BuildContext context, double percent, double maxValue) {
  double rWidth;
  double preWidth = MediaQuery
      .of(context)
      .size
      .width;
  double preHeight = MediaQuery
      .of(context)
      .size
      .height;
  if (preWidth > preHeight) {
    rWidth = preHeight * percent;
  } else
    rWidth = preWidth * percent;

  if (maxValue != null) {
    return (rWidth >= maxValue) ? maxValue : rWidth;
  } else
    return rWidth;
}

double getHeight(BuildContext context, double percent, double maxValue) {
  double rHeight;
  double preWidth = MediaQuery
      .of(context)
      .size
      .width;
  double preHeight = MediaQuery
      .of(context)
      .size
      .height;
  if (preWidth > preHeight) {
    rHeight = preWidth * percent;
  } else
    rHeight = preHeight * percent;

  if (maxValue != null) {
    return (rHeight >= maxValue) ? maxValue : rHeight;
  } else
    return rHeight;
}

Future<String> getPublicIP() async {
  try {
    const url = 'https://api.ipify.org';
    var response = await http.get(url);
    if (response.statusCode == 200) {
      // The response body is the IP in plain text, so just
      // return it as-is.
      return response.body;
    } else {
      // The request failed with a non-200 code
      // The ipify.org API has a lot of guaranteed uptime
      // promises, so this shouldn't ever actually happen.
      print(response.statusCode);
      print(response.body);
      return null;
    }
  } catch (e) {
    // Request failed due to an error, most likely because
    // the phone isn't connected to the internet.
    print(e);
    return null;
  }
}

TimeOfDay getTimeOfDay(String s) =>
    TimeOfDay(
        hour: int.parse(s.split(":")[0]), minute: int.parse(s.split(":")[1]));

String getStringTimeOfDay(TimeOfDay timeDay) {
  var hours = '';
  var minutes = '';
  var time = '';
  hours = (timeDay.hour < 10)
      ? '0' + timeDay.hour.toString()
      : timeDay.hour.toString();
  minutes = (timeDay.minute < 10)
      ? '0' + timeDay.minute.toString()
      : timeDay.minute.toString();
  time = hours + ':' + minutes;
  return time;
}

double toDouble(TimeOfDay myTime,{double ext = 0}) => myTime.hour + myTime.minute / 60.0 + ext;



double getPercentWidth(BuildContext context, double percent, double maxValue) {
  double rWidth;
  double preWidth = MediaQuery.of(context).size.width;
  double preHeight = MediaQuery.of(context).size.height;
  if (preWidth > preHeight) {
    rWidth = preHeight * percent;
  } else
    rWidth = preWidth * percent;

  if (maxValue != null) {
    return (rWidth >= maxValue) ? maxValue : rWidth;
  } else
    return rWidth;
}

double getPercentHeight(BuildContext context, double percent, double maxValue) {
  double rHeight;
  double preWidth = MediaQuery.of(context).size.width;
  double preHeight = MediaQuery.of(context).size.height;
  if (preWidth > preHeight) {
    rHeight = preWidth * percent;
  } else
    rHeight = preHeight * percent;

  if (maxValue != null) {
    return (rHeight >= maxValue) ? maxValue : rHeight;
  } else
    return rHeight;
}