import '../all_file.dart';

class CustomButton extends StatefulWidget{
  @required VoidCallback onPressed;
  String text;
  IconData icon;
  Color color;
  double height;
  double width;
  CustomButton({this.text, this.color, this.icon, this.width, this.height,this.onPressed});
  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: widget.width,
      height: widget.height,
      child: RaisedButton(
        onPressed: widget.onPressed,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: <Widget>[
            Center(
              child: Text(
                widget.text,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                ),
              ),
            ),
            widget.icon != null?Icon(
              widget.icon,
              color: Colors.white,
              size: 20,
            ):Container()
          ],
        ),
        color: widget.color,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(15.0),
        ),
      ),
    );
  }
}