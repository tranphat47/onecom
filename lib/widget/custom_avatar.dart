import '../all_file.dart';

class CustomAvatar extends StatefulWidget {
  final String avatarUrl;
  final bool picker;
  final String defaultSvg;
  final double size;
  final Color color;
  String tag;

  CustomAvatar(
      {this.picker = false,
      this.avatarUrl,
      this.defaultSvg,
      this.tag,
      this.size,
      this.color});

  @override
  _CustomAvatarState createState() => _CustomAvatarState();
}

class _CustomAvatarState extends State<CustomAvatar> {
  Color color;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    color = (widget.color == null) ? myTheme.greenSecondary : widget.color;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: fileBloc.changeStream,
        builder: (context, snapshot) {
          return GestureDetector(
            onTap: () {
              if (widget.picker) fileBloc.getImage();
            },
            child: ClipOval(
              child: Container(
                padding: const EdgeInsets.all(1),
                color: color,
                child: ClipOval(
                  child: Container(
                    child: (fileBloc.image != null && widget.picker)
                        ? Image.file(
                            fileBloc.image,
                            fit: BoxFit.cover,
                            width: widget.size - 1,
                            height: widget.size - 1,
                          )
                        : (widget.avatarUrl == null)
                            ? SvgPicture.asset(
                                'assets/svg/${widget.defaultSvg}.svg',
                              )
//                            : CachedNetworkImage(
//                                imageUrl: widget.avatarUrl,
//                                fit: BoxFit.fill,
//                                placeholder: (context, url) => SvgPicture.asset(
//                                  'assets/svg/${widget.defaultSvg}.svg',
//                                ),
//                                errorWidget: (context, url, error) =>
//                                    SvgPicture.asset(
//                                  'assets/svg/${widget.defaultSvg}.svg',
//                                ),
//                              ),
                            : Image.network(
                                widget.avatarUrl,
                                fit: BoxFit.cover,
                                width: widget.size - 1,
                                height: widget.size - 1,
                              ),
                    width: widget.size - 1,
                    height: widget.size - 1,
                  ),
                ),
              ),
            ),
          );
        });
  }
}
