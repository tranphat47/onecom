import '../all_file.dart';

class MyMaterialButton extends StatefulWidget {
  final double width;
  final double height;
  final callback;
  final String label;
  final colors;
  final margin;
  final childWidget;
  final double borderRadius;
  final double maxHeight;
  final bool showArrow;
  final double textSize;
  final bool upcaseText;

  MyMaterialButton({
    this.width,
    this.height,
    this.callback,
    this.label,
    this.colors,
    this.margin,
    this.childWidget,
    this.borderRadius,
    this.maxHeight,
    this.showArrow = false,
    this.textSize = 18,
    this.upcaseText = true,
  });

  @override
  _MyMaterialButtonState createState() => _MyMaterialButtonState();
}

class _MyMaterialButtonState extends State<MyMaterialButton> {
  @override
  Widget build(BuildContext context) {
    double borderRadius =
        (widget.borderRadius == null) ? 5.0 : widget.borderRadius;
    double marginB = (widget.margin == null) ? 0 : widget.margin;
    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        width: widget.width,
        height: widget.height,
        margin: EdgeInsets.fromLTRB(0, marginB, 0, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
          color: widget.colors,
        ),
        child: (widget.childWidget == null)
            ? (widget.showArrow)
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: widget.width / 5,
                      ),
                      Text(
                        Languages.get(widget.label).toUpperCase(),
                        style: TextStyle(
                            color: Colors.white, fontSize: widget.textSize),
                      ),
                      Container(
                        width: widget.width / 5,
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  )
                : Center(
                    child: (widget.upcaseText)
                        ? Text(
                            Languages.get(widget.label).toUpperCase(),
                            style: TextStyle(
                                color: Colors.white, fontSize: widget.textSize),
                          )
                        : Text(
                            Languages.get(widget.label),
                            style: TextStyle(
                                color: Colors.white, fontSize: widget.textSize),
                          ),
                  )
            : widget.childWidget,
      ),
    );
  }
}
