import '../all_file.dart';
import 'package:flutter/cupertino.dart';

class CustomDatePicker extends StatefulWidget {
  @override
  _CustomDatePickerState createState() => _CustomDatePickerState();
}

class _CustomDatePickerState extends State<CustomDatePicker> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        DatePicker.showDatePicker(
          context,
          showTitleActions: true,
          currentTime: dateBloc.selectedDate,
          locale: LocaleType.vi,
          minTime: dateBloc.selectedDate.subtract(Duration(days: 50000)),
          maxTime: dateBloc.selectedDate.add(Duration(days: 50000)),
          theme: DatePickerTheme(
              headerColor: Colors.white,
              backgroundColor: Colors.white,
              itemStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  fontSize: 18),
              cancelStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
              doneStyle: TextStyle(
                  color: myTheme.greenPrimary,
                  fontSize: 18,
                  fontWeight: FontWeight.w500)),
          onConfirm: (date) {
            dateBloc.setSelectedDate(date);
          },
        );
      },
      child: StreamBuilder(
          stream: dateBloc.changeStream,
          builder: (context, snapshot) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  (dateBloc.selectedDate == null)
                      ? 'Chọn ngày'
                      : dateBloc.getSelectedDateString,
                  textAlign: TextAlign.end,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                     Padding(
                        padding: EdgeInsets.only(left: 5),
                        child: Icon(Icons.arrow_forward_ios, size: 15,),
                      )
              ],
            );
          }),
    );
  }
}
