import 'package:one_com/widget/time_picker_end.dart';

import '../all_file.dart';
import 'date_picker.dart';

enum CustomTextFieldType {
  DEFAULT,
  DATE_PICKER,
  TIME_PICKER,
  TIME_PICKER_END,
}

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final Stream stream;
  final double paddingTop;
  final TextInputType keyboardType;
  final CustomTextFieldType customTextFieldType;
  final onChanged;
  final bool required;
  final Widget prefix;
  final bool readOnly;
  final scaffoldKey;
  final bool shortField;

  CustomTextField(
      {this.hint,
      this.controller,
      this.scaffoldKey,
      this.onChanged,
      this.readOnly = false,
      this.required = false,
      this.label,
      this.prefix,
      this.stream,
      this.keyboardType = TextInputType.text,
      this.customTextFieldType = CustomTextFieldType.DEFAULT,
      this.paddingTop = 10,
      this.shortField = true});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    switch (widget.customTextFieldType) {
      case CustomTextFieldType.DEFAULT:
        return _buildDefaultTextField();
      case CustomTextFieldType.DATE_PICKER:
        return _buildDatePickerField();
      case CustomTextFieldType.TIME_PICKER:
        return _buildTimePickerField();
      case CustomTextFieldType.TIME_PICKER_END:
        return _buildTimePickerEndField();
    }
  }

  Widget _buildDefaultTextField() {
    return Container(
      width: (widget.shortField)
          ? getWidth(context, 0.42, null)
          : getWidth(context, 0.95, null),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: widget.paddingTop),
            child: RichText(
              text: TextSpan(
                  text: Languages.get(widget.label),
                  style: TextStyle(
                      fontSize: 16,
                      color: myTheme.greenPrimary,
                      fontWeight: FontWeight.w700),
                  children: [
                    TextSpan(
                        text: (widget.required) ? '*' : '',
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.red,
                            fontWeight: FontWeight.w700))
                  ]),
            ),
          ),
          (widget.stream != null)
              ? StreamBuilder(
                  stream: widget.stream,
                  builder: (context, snapshot) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            (widget.prefix != null)
                                ? widget.prefix
                                : Container(),
                            Expanded(
                              child: TextFormField(
                                controller: widget.controller,
                                keyboardType: widget.keyboardType,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.zero,
                                  hintText: Languages.get(widget.hint),
                                  focusedBorder: new UnderlineInputBorder(
                                    borderSide: new BorderSide(
                                      color: myTheme.greenPrimary,
                                    ),
                                  ),
                                  enabledBorder: new UnderlineInputBorder(
                                    borderSide: new BorderSide(
                                      color: myTheme.greenPrimary,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          (snapshot.hasError) ? snapshot.error : '',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    );
                  })
              : Row(
                  children: <Widget>[
                    (widget.prefix != null) ? widget.prefix : Container(),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            controller: widget.controller,
                            keyboardType: widget.keyboardType,
                            onChanged: widget.onChanged,
                            decoration: InputDecoration(
                              hintText: Languages.get(widget.hint),
                              hintStyle: TextStyle(fontSize: 13),
                              focusedBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                  color: myTheme.greenPrimary,
                                ),
                              ),
                              enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                  color: myTheme.gray,
                                ),
                              ),
                            ),
                          ),
                          Text(
                             '',
                          )
                        ],
                      ),
                    ),
                  ],
                ),
        ],
      ),
    );
  }

  Widget _buildDatePickerField() {
    return Container(
      width: getWidth(context, 0.42, null),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RichText(
                text: TextSpan(
                    text: Languages.get(widget.label),
                    style: TextStyle(
                        fontSize: 16,
                        color: myTheme.greenPrimary,
                        fontWeight: FontWeight.w700),
                    children: [
                      TextSpan(
                          text: (widget.required) ? '*' : '',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.red,
                              fontWeight: FontWeight.w700))
                    ]),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25, bottom: 15),
            child: CustomDatePicker(),
          ),
          (widget.stream != null)
              ? StreamBuilder(
                  stream: widget.stream,
                  builder: (context, snapshot) {
                    return Text(
                      (snapshot.hasError) ? snapshot.error : '',
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    );
                  })
              : Container()
        ],
      ),
    );
  }

  Widget _buildTimePickerField() {
    return Container(
      width: getWidth(context, 0.42, null),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RichText(
                text: TextSpan(
                    text: Languages.get(widget.label),
                    style: TextStyle(
                        fontSize: 16,
                        color: myTheme.greenPrimary,
                        fontWeight: FontWeight.w700),
                    children: [
                      TextSpan(
                          text: (widget.required) ? '*' : '',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.red,
                              fontWeight: FontWeight.w700))
                    ]),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 5),
            child: CustomTimePicker(),
          ),
          (widget.stream != null)
              ? StreamBuilder(
                  stream: widget.stream,
                  builder: (context, snapshot) {
                    return Text(
                      (snapshot.hasError) ? snapshot.error : '',
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    );
                  })
              : Container()
        ],
      ),
    );
  }

  Widget _buildTimePickerEndField() {
    return Container(
      width: getWidth(context, 0.42, null),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RichText(
                text: TextSpan(
                    text: Languages.get(widget.label),
                    style: TextStyle(
                        fontSize: 16,
                        color: myTheme.greenPrimary,
                        fontWeight: FontWeight.w700),
                    children: [
                      TextSpan(
                          text: (widget.required) ? '*' : '',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.red,
                              fontWeight: FontWeight.w700))
                    ]),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 5),
            child: CustomTimeEndPicker(),
          ),
          (widget.stream != null)
              ? StreamBuilder(
                  stream: widget.stream,
                  builder: (context, snapshot) {
                    return Text(
                      (snapshot.hasError) ? snapshot.error : '',
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    );
                  })
              : Container()
        ],
      ),
    );
  }
}
