import '../all_file.dart';

class HomeHeaderExpanded extends StatelessWidget {
  HomeHeaderExpanded({
    Key key,
    this.title = '',
    this.actions,
  }) : super(key: key);
  final String title;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: 120.0,
      floating: false,
      pinned: true,
      elevation: 1.0,
      actions: this.actions,
      backgroundColor: myTheme.greenPrimary,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: false,
        titlePadding: EdgeInsets.only(left: 20.0, bottom: 15),
        title: SafeArea(
          right: false,
          child: Container(
            margin: EdgeInsets.only(left: 3),
            child: Text(
              this.title,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
        background: Container(
          color: myTheme.greenPrimary,
        ),
      ),
    );
  }
}
