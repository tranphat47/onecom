import 'package:dio/dio.dart';
import '../all_file.dart';

class TeamApi {
  Dio dio = new Dio(BaseOptions(
    baseUrl: AppConfig.SERVER_URL,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  ));

  createTeam({
      String teamName,
      String teamCode,
      String username,
      String password,
      String firstName,
      String lastName,
  }) async {
    final apiUrl = '/create_team';
    final FormData data = new FormData.fromMap({
      'teamName': teamName,
      'teamCode': teamCode,
      'username': username,
      'password': password,
      'firstName': firstName,
      'lastName': lastName,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  updateTeam({
    String id,
    String teamName,
    String teamCode,
    String email,
    String phone,
    String address,
    int timeLimit,
    String ipWifi,
  }) async {
    final apiUrl = '/update_team/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
      'teamName': teamName,
      'teamCode': teamCode,
      'email': email,
      'phone': phone,
      'address': address,
      'timeLimit': timeLimit,
      'ipWifi': ipWifi,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  updateTimeLimit({
    String id,
    int timeLimit,
  }) async {
    final apiUrl = '/update_timelimit/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
      'timeLimit': timeLimit,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
