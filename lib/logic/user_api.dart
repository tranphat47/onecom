import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:one_com/bloc/reload_bloc.dart';
import 'package:one_com/model/config/config_model.dart';
import 'package:one_com/model/team/team_model.dart';
import '../all_file.dart';

class UserApi {
  Dio dio = new Dio(BaseOptions(
    baseUrl: AppConfig.SERVER_URL,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  ));

  ///-------login, logout, reload--------////
  login({String name, String pass}) async {
    String url = '/login';
    final FormData data = new FormData.fromMap({
      "username": name,
      "password": pass,
    });

    // User login
    try {
      Response response = await dio.post(url, data: data);
      if (response.data['success']) {
        var data = response.data['data'];

        appState.prefs.setString('username', name);
        appState.prefs.setString('password', pass);
        appState.prefs.setString('last_login', json.encode(data));
        appState.token = data['token'];
        appState.currentUser = UserModel.fromJson(data['user_info']);
        appState.teamInfo = TeamModel.fromJson(data['team_info']);
        if (data['my_registrations_today'] != null) {
          appState.regisToday =
              RegisShiftModel.fromJson(data['my_registrations_today']);
        } else
          appState.regisToday = null;

        appState.listNews = (data['list_news'] as List)
            .map((i) => NewsModel.fromJson(i))
            .cast<NewsModel>()
            .toList();
        appState.listUser = (data['list_user'] as List)
            .map((i) => UserModel.fromJson(i))
            .cast<UserModel>()
            .toList();
        appState.listShiftwork = (data['list_shiftwork'] as List)
            .map((i) => ConfigModel.fromJson(i))
            .cast<ConfigModel>()
            .toList();
        appState.listShiftRegis = (data['my_registrations'] as List)
            .map((i) => RegisShiftModel.fromJson(i))
            .cast<RegisShiftModel>()
            .toList();
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  logout() {
    appState.prefs.remove('username');
    appState.prefs.remove('password');
    appState.prefs.remove('lastlogin');
  }

  reload() async {
    await login(
        name: appState.prefs.getString('username'),
        pass: appState.prefs.getString('password'));
    reloadBloc.reload();
  }

  ///----------////

  createUser({
    String firstName,
    String lastName,
    DateTime dateOfBirth,
    String email,
    String phone,
    File avataUrl,
    String jobTitle,
    String username,
    String password,
    String team_id,
  }) async {
    final url = '/create_user';
    String image64;
    if (avataUrl != null) {
      List<int> imageBytes = await avataUrl.readAsBytes();
      image64 = base64Encode(imageBytes);
    }
    final FormData data = new FormData.fromMap({
      'firstName': firstName,
      'lastName': lastName,
      'dateOfBirth': dateOfBirth,
      'email': email,
      'phone': phone,
      'avataUrl': image64,
      'jobTitle': jobTitle,
      'username': username,
      'password': password,
      'team_id': team_id,
    });
    try {
      Response response = await dio.post(url,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  updateUser({
    String id,
    String firstName,
    String lastName,
    DateTime dateOfBirth,
    String email,
    String phone,
    File avataUrl,
    String jobTitle,
  }) async {
    final url = '/update_user/$id';
    FormData data;
    String image64;
    if (avataUrl != null) {
      List<int> imageBytes = await avataUrl.readAsBytes();
      image64 = base64Encode(imageBytes);
      data = new FormData.fromMap({
        'id': id,
        'firstName': firstName,
        'lastName': lastName,
        'dateOfBirth': dateOfBirth,
        'email': email,
        'phone': phone,
        'avataUrl': image64,
        'jobTitle': jobTitle,
      });
    } else {
      data = new FormData.fromMap({
        'id': id,
        'firstName': firstName,
        'lastName': lastName,
        'dateOfBirth': dateOfBirth,
        'email': email,
        'phone': phone,
        'jobTitle': jobTitle,
      });
    }
    try {
      Response response = await dio.post(url,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  deleteUser({String id}) async {
    final url = '/delete_user/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
    });
    try {
      Response response = await dio.post(url,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  changePassword({String old_password, String new_password}) async {
    final url = '/change_password';
    final FormData data = new FormData.fromMap({
      'old_password': old_password,
      'new_password': new_password,
    });
    try {
      Response response = await dio.post(url,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['status'] == 200) {
        appState.log = response.data['message'];
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
