import 'package:dio/dio.dart';
import '../all_file.dart';

class ShiftWork {
  Dio dio = new Dio(BaseOptions(
    baseUrl: AppConfig.SERVER_URL,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  ));

  createShiftwork({
    String shiftworkName,
    DateTime begin,
    DateTime end,
    String team_id,
  }) async {
    final apiUrl = '/create_shiftwork';
    final FormData data = new FormData.fromMap({
      'shiftworkName': shiftworkName,
      'begin': begin,
      'end': end,
      'team_id': team_id,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        var data = response.data['data'];
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  updateShiftwork({
    String id,
    String shiftworkName,
    DateTime begin,
    DateTime end,
  }) async {
    final apiUrl = '/update_shiftwork/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
      'shiftworkName': shiftworkName,
      'begin': begin,
      'end': end,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  deleteShiftwork({String id}) async {
    final url = '/delete_shiftwork/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
    });
    try {
      Response response = await dio.post(url,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  createRegisShift({
    String user_id,
    String shiftwork_id,
    String shiftworkName,
    DateTime date,
  }) async {
    final apiUrl = '/create_registration';
    final FormData data = new FormData.fromMap({
      'user_id': user_id,
      'shiftwork_id': shiftwork_id,
      'shiftworkName': shiftworkName,
      'date': date,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  updateRegisShift({
    String id,
    int isCheckin,
    int isCheckout,
  }) async {
    final apiUrl = '/update_registration/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
      'isCheckin': isCheckin,
      'isCheckout': isCheckout,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  deleteRegisShift({String id}) async {
    final url = '/delete_registration/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
    });
    try {
      Response response = await dio.post(url,
          data: data,
          options:
          Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
